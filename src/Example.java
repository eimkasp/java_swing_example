import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class Example {
    public JPanel panel1;
    private JButton pasisveikiniButton;
    private JLabel counter;
    private JComboBox comboBox1;
    private JTextField labasField;
    private JTabbedPane tabbedPane1;

    private Integer count = 0;


    List<Darbuotojas> darbutojaiData = Darbuotojas.getAll();




    String col[] = {"ID","Team","P", "W", "L", "D"};

    DefaultTableModel tableModel = new DefaultTableModel(col, 0);
    // The 0 argument is number rows.

    JTable table1 = new JTable(tableModel);



    public Example() {

        addDataToTable();
        table1.setAutoCreateRowSorter(true);
        panel1.add(table1);
        pasisveikiniButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Paspaudus mygtuka vykdysime kodas kuris yra aprasytas cia

                count++;

                counter.setText("Jus paspaudete mygtuka " + count.toString() + " kartu");


                // Iskvieciam message dialog
                JOptionPane.showMessageDialog(null, comboBox1.getSelectedItem());
            }
        });
        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                labasField.setText(comboBox1.getSelectedItem().toString());
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    private void addDataToTable() {
        for(Darbuotojas darbuotojas:darbutojaiData) {
            Object[] objs = {darbuotojas.id, darbuotojas.name, darbuotojas.surname, darbuotojas.education, darbuotojas.salary};

            tableModel.addRow(objs);
        }
    }

}
