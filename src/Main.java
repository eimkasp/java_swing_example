import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        /* Sukuriame frame objekta ir parodome varotojui */
        JFrame frame = new JFrame("App");
        frame.setContentPane(new Example().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }
}
